#!/usr/bin/env python3

from datetime import date
import json
import os
from pathlib import Path
import subprocess
import tempfile

import requests
from furl import furl

gitlab_server_url = os.environ["CI_SERVER_URL"]
personal_access_token = os.environ["GITLAB_BOT_PERSONAL_ACCESS_TOKEN"].strip().split(':')

api_loc = gitlab_server_url + "/api/v4"

# logging http requests
# import logging
# logging.basicConfig(level=logging.DEBUG)

access_token_headers = { "PRIVATE-TOKEN": personal_access_token[1] }


def invoke_command(cmd):
    process = subprocess.Popen(cmd, shell=True,
                 stdout=subprocess.PIPE,
                 stderr=subprocess.STDOUT)
    output = process.communicate()[0]
    if process.returncode != 0:
        print("'{}' failed ({}):\n{}\n".format(
            cmd, process.returncode, output.decode("utf-8")));
    assert process.returncode == 0

def invoke_command_and_collect_output(cmd):
    process = subprocess.Popen(cmd, shell=True,
                 stdout=subprocess.PIPE)
    output = process.communicate()[0]
    assert process.returncode == 0
    return output.decode("utf-8")


def add_access_token_to_remote_url(remote_url):
    assert (remote_url.startswith('https'))
    furl_url = furl(remote_url)

    furl_url.username = personal_access_token[0]
    furl_url.password = personal_access_token[1]

    return furl_url.tostr()

def create_or_update_flake_auto_update_merge_request(api_loc, project_id, personal_access_token, default_branch):

    access_token_headers = { "PRIVATE-TOKEN": personal_access_token[1] }

    # get the network location of the GitLab instance's API based on $HOST
    project_api_loc = api_loc + "/projects"
    project_api_endpoint = project_api_loc + "/" + project_id

    invoke_command('git update-index --refresh')
    invoke_command('git diff-index --exit-code HEAD --')

    auto_update_branch_name = 'flake-auto-update'
    invoke_command('git checkout {}'.format(default_branch))
    invoke_command('git branch -f {}'.format(auto_update_branch_name))

    invoke_command('git checkout {}'.format(auto_update_branch_name))

    invoke_command('nix flake update --commit-lock-file')

    # TODO: stop and do nothing if our update is not meaningful
    generated_diff = invoke_command_and_collect_output(
            "git diff {}".format(default_branch))
    would_change_default = not generated_diff.isspace()

    generated_diff = invoke_command_and_collect_output(
            "git diff origin/{} -- || echo non-empty".format(auto_update_branch_name))
    would_change_auto_update = not generated_diff.isspace()

    if ((not would_change_default) and (not would_change_auto_update)):
        return

    # TODO: mabe use --force-with-lease?
    invoke_command('git push --force origin {}'.format(auto_update_branch_name))

    merge_request_description = invoke_command_and_collect_output('git show -s --format=%B')

    matching_merge_requests = requests.get(
            project_api_endpoint + '/merge_requests?state=opened&labels=flake-auto-update',
            headers=access_token_headers).json()

    assert (len(matching_merge_requests) < 2)

    existing_open_merge_request = matching_merge_requests[0] if (len(matching_merge_requests) == 1) else None

    merge_request_title = 'Updating flake.lock on ' + date.today().strftime('%Y-%m-%d')

    if existing_open_merge_request:
        print('updating existing merge request ...')
        iid = existing_open_merge_request['iid']
        merge_request_change_metadata = {
            'id': project_id,
            'merge_request_iid': iid,
            'title': merge_request_title,
            'description': merge_request_description
        }

        response = requests.put(
                project_api_endpoint + '/merge_requests/' + str(iid),
                json=merge_request_change_metadata,
                headers=access_token_headers)

        assert(response.status_code == 200)
        print('done')
    else:
        print('creating a new merge request ...')
        # Assemble the metadata for a new merge request
        merge_request_metadata = {
            'id': project_id,
            'source_branch': auto_update_branch_name,
            'target_branch': default_branch,
            'title': merge_request_title,
            'description': merge_request_description,
            'labels': ['flake-auto-update'],
            'remove_source_branch': True,
            'allow-colaboration': True
        }

        response = requests.post(
                project_api_endpoint + '/merge_requests',
                json=merge_request_metadata,
                headers=access_token_headers)

        assert(response.status_code == 201)

        print('done')


# get all of the projects where the current user has at least reporter or developer level access

# this means we can push code and open merge requests
# potentially we could get by with reporter permissions
# if we create a fork instead of pushing the original repo,
# but I am not sure if that's worth it and doesn't come with
# its own projects

access_level_developer = 30
projects = requests.get(
        api_loc + '/projects?min_access_level={}'.format(access_level_developer),
        headers=access_token_headers
        ).json()

script_working_directory = os.getcwd()

for project in projects:
    project_id = str(project['id'])
    project_name = project['path']
    http_url = project['http_url_to_repo']
    default_branch = project['default_branch']

    # limit scope for testing
    # if (project_name != 'auto-update-test-setup' and project_name != 'gradle2nix'): continue;

    print("project_id: {}\nproject_name: {}\nproject_url: {}\ndefault_branch: {}\n".format(
        project_id, project_name, http_url, default_branch))

    # check if project has a flake file
    flake_file_metadata = requests.head(
        api_loc + '/projects/{}/repository/files/flake%2Enix?ref={}'.format(project_id, default_branch),
        headers=access_token_headers
        )

    if (flake_file_metadata.status_code != 200):
        if (flake_file_metadata.status_code == 404):
            print("skipping project - not a flake")
            continue;
        else:
            # this should not happen
            assert(false)

    # we know we have a flake now
    with tempfile.TemporaryDirectory(suffix=project_name) as tmp_dir:
        print("tmp_dir: " + tmp_dir)

        os.chdir(tmp_dir)

        # clone the repository
        http_url_with_access_token = add_access_token_to_remote_url(http_url)
        invoke_command('git clone {} .'.format(http_url_with_access_token))

        create_or_update_flake_auto_update_merge_request(api_loc, project_id, personal_access_token, default_branch)

        os.chdir(script_working_directory)

        # tmp_dir is cleared automatically after with here

