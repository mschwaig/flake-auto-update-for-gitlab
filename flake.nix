

{
  description = "flake auto update for gitlab";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
  utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { inherit system; };
    in {
      defaultPackage = pkgs.stdenv.mkDerivation {
        name = "flake-auto-update-for-gitlab";
        buildInputs = [
          (pkgs.python38.withPackages (pythonPackages: with pythonPackages; [
            furl requests
          ]))
        ];
        unpackPhase = "true";
        installPhase = ''
          mkdir -p $out/bin
          cp ${./flake-auto-update-for-gitlab.py} $out/bin/flake-auto-update-for-gitlab
          chmod +x $out/bin/flake-auto-update-for-gitlab
        '';
      };
    });
  }
